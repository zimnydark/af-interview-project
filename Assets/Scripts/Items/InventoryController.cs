﻿namespace AFSInterview.Items
{
	using AFSInterview.Singletons;
	using AFSInterview.UI.HUD;
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	public class InventoryController : SingletonMonoBehavior<InventoryController>
	{
		[SerializeReference] private List<Item> items;
		[SerializeField] private int initMoneyValue;

		private int money = 0;

		public int Money
		{
			get => money;

			set
			{
				money = value;
				OnMoneyValueChanged?.Invoke(money);
			}
		}
		public int ItemsCount => items.Count;

		public event Action<int> OnMoneyValueChanged;

		private void Start()
		{
			Money = initMoneyValue;
		}

		public void SellAllItemsUpToValue(int maxValue)
		{
			for (var i = items.Count - 1; i>=0; i--)
			{
				var itemValue = items[i].Value;
				if (itemValue > maxValue)
					continue;

				SellItem(items[i]);
				items.RemoveAt(i);
			}

		}

		public void SellItem(Item item)
		{
			Money += item.Value;	
		}

		public void AddItem(Item item)
		{
			items.Add(item);
		}
	}
}