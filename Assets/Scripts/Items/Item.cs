﻿namespace AFSInterview.Items
{
	using System;
	using UnityEngine;

	[Serializable]
	public abstract class Item
	{
		[SerializeField] protected string name;
		[SerializeField] protected int value;

		public string Name => name;
		public int Value => value;

		public Item()
		{

		}

		public Item(string name, int value)
		{
			this.name = name;
			this.value = value;
		}

		protected Item(Item copyThis) : this()
		{
			this.name = copyThis.name;
			this.value = copyThis.value;
		}

		public abstract Item Clone();

		public virtual void Use()
		{
			Debug.Log("Using " + Name);
		}
	}

	[Serializable]
	public class SellableItem : Item
	{
		public SellableItem() : base()
		{

		}

		public SellableItem(string name, int value) : base(name, value)
		{
		}

		protected SellableItem(SellableItem copyThis) : base(copyThis)
		{

		}

		public override Item Clone()
		{
			return new SellableItem(this);
		}

		public override void Use()
		{
			base.Use();
			InventoryController.Instance?.SellItem(this);
		}
	}

	[Serializable]
	public class Exchangeable : Item
	{
		public Exchangeable() : base()
		{

		}

		public Exchangeable(string name, int value) : base(name, value)
		{

		}

		protected Exchangeable(Exchangeable copyThis) : base(copyThis)
		{

		}

		public override Item Clone()
		{
			return new Exchangeable(this);
		}

		public override void Use()
		{
			base.Use();
			ItemsManager.Instance?.SpawnRandomItem();
		}
	}
}