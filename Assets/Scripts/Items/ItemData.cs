using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AFSInterview.Items
{
	[CreateAssetMenu(fileName = "ItemData", menuName = "Items/ItemData", order = 1)]
	public class ItemData : ScriptableObject
	{
		[SerializeReference] private List<Item> items = new List<Item>();

		public Item GetRandomItem()
		{
			return items[Random.Range(0, items.Count)];
		}
    }
}
