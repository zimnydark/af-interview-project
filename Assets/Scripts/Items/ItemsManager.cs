﻿namespace AFSInterview.Items
{
	using AFSInterview.Singletons;
	using UnityEngine;

	public class ItemsManager : SingletonMonoBehavior<ItemsManager>
	{
		[SerializeField] private ItemData itemData;
		[SerializeField] private int itemSellMaxValue;
		[SerializeField] private Transform itemSpawnParent;
		[SerializeField] private ItemPresenter itemPrefab;
		[SerializeField] private BoxCollider itemSpawnArea;
		[SerializeField] private float itemSpawnInterval;

		private float nextItemSpawnTime;

		public void SpawnRandomItem()
		{
			var spawnAreaBounds = itemSpawnArea.bounds;
			var position = new Vector3(
				Random.Range(spawnAreaBounds.min.x, spawnAreaBounds.max.x),
				0f,
				Random.Range(spawnAreaBounds.min.z, spawnAreaBounds.max.z)
			);

			var item = Instantiate(itemPrefab, position, Quaternion.identity, itemSpawnParent);
			item.LoadItem(itemData.GetRandomItem());
		}

		private void Update()
		{
			if (Time.time >= nextItemSpawnTime)
				SpawnNewItem();

			if (Input.GetMouseButtonDown(0))
				TryPickUpItem();

			if (Input.GetMouseButtonDown(1))
				TryUseItem();

			if (Input.GetKeyDown(KeyCode.Space))
				InventoryController.Instance?.SellAllItemsUpToValue(itemSellMaxValue);
		}

		private void SpawnNewItem()
		{
			nextItemSpawnTime = Time.time + itemSpawnInterval;

			SpawnRandomItem();
		}

		private void TryPickUpItem()
		{
			var itemHolder = TryGetItem();
			if (itemHolder == null) return;

			var item = itemHolder.GetItem(true);
			InventoryController.Instance?.AddItem(item);
			Debug.Log("Picked up " + item.Name + " with value of " + item.Value + " and now have " + InventoryController.Instance?.ItemsCount + " items");
		}

		private void TryUseItem()
		{
			var itemHolder = TryGetItem();
			if (itemHolder == null) return;

			var item = itemHolder.GetItem(true);
			item.Use();
		}

		private IItemHolder TryGetItem()
		{
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			var layerMask = LayerMask.GetMask("Item");
			if (!Physics.Raycast(ray, out var hit, 100f, layerMask) || !hit.collider.TryGetComponent<IItemHolder>(out var itemHolder))
				return null;

			return itemHolder;
		}
	}
}