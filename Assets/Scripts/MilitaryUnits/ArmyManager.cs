using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AFSInterview.MilitaryUnits
{
	public enum ArmyTeam
	{
		Left,
		Right
	}

	public class ArmyManager : MonoBehaviour
	{
		private float DEFAULT_PAUSE_BETWEEN_ATTACKS = 3f;

		[SerializeField, BoxGroup("Settings")] private MilitaryEntitiesData entitiesData;
		[SerializeField, BoxGroup("Settings")] private BoxCollider spawnArea;

		[SerializeField, BoxGroup("Instance")] private List<MilitaryType> armyData = new List<MilitaryType>();
		[SerializeField, BoxGroup("Instance")] private ArmyTeam team;

	
		[ShowInInspector] private List<MilitaryEntity> militaryEntities = new List<MilitaryEntity>();

		public ArmyTeam Team => team;

		public event Action OnMakeMove;

		public void Initialize()
		{
			militaryEntities.Shuffle();
			militaryEntities.ForEach(e =>
			{
				e.OnEntityDied += OnEntityDied;
			});
		}

		public void MakeMove()
		{
			StartCoroutine(AllArmyMakeMove());
		}

		public MilitaryEntity GetRandomEntity()
		{
			if(militaryEntities.Count < 1)
			{
				return null;
			}

			System.Random rnd = new System.Random();
			
			int index = rnd.Next(militaryEntities.Count);

			return militaryEntities[index];
		}

		public bool CheckIsAnyAlive()
		{
			return militaryEntities.Count > 0;
		}

		public void FinishGame()
		{
			StopAllCoroutines();
		}

		private void Awake()
		{
			armyData.ForEach(e =>
			{
				var prefab = entitiesData.GetPrefabToSpawn(e);

				var spawnAreaBounds = spawnArea.bounds;
				var position = new Vector3(
					UnityEngine.Random.Range(spawnAreaBounds.min.x, spawnAreaBounds.max.x),
					0f,
					UnityEngine.Random.Range(spawnAreaBounds.min.z, spawnAreaBounds.max.z)
				);

				var entity = Instantiate(prefab, position, Quaternion.identity, this.transform);

				militaryEntities.Add(entity);
			});
		}

		private void OnEnable()
		{
			CombatManager.Instance?.RegisterArmy(this);
		}

		private void OnDisable()
		{
			CombatManager.Instance?.UnregisterArmy(this);
		}

		private void OnEntityDied(MilitaryEntity entity)
		{
			entity.OnEntityDied -= OnEntityDied;
			militaryEntities.Remove(entity);

			Destroy(entity.gameObject);

			if(militaryEntities.Count < 1)
			{
				CombatManager.Instance?.FinishGame();
			}
		}

		private IEnumerator AllArmyMakeMove()
		{
			var iterator = 0;
			while (iterator < militaryEntities.Count)
			{
				yield return new WaitForSeconds(DEFAULT_PAUSE_BETWEEN_ATTACKS);

				if (militaryEntities[iterator].CanAttack())
				{
					var enemyEntity = CombatManager.Instance.GetRandomEnemyEntity(Team);
					if (enemyEntity != null)
					{
						militaryEntities[iterator].MakeDamage(enemyEntity);
					}
				}

				iterator++;
			}
			FinishTurn();
		}

		private void FinishTurn()
		{
			militaryEntities.ForEach(e => e.FinishTurn());
			OnMakeMove?.Invoke();
			Debug.Log(Team + " Team finished turn");
		}
	}
}
