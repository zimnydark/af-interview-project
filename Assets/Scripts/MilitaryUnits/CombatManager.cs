using AFSInterview.Singletons;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AFSInterview.MilitaryUnits
{
	public class CombatManager : SingletonMonoBehavior<CombatManager>
	{
		private List<ArmyManager> armies = new List<ArmyManager>();

		private ArmyManager armyTurn = null;

		public void RegisterArmy(ArmyManager army)
		{
			armies.Add(army);
			army.OnMakeMove += OnMakeMove;
		}

		public void UnregisterArmy(ArmyManager army)
		{
			army.OnMakeMove -= OnMakeMove;
			armies.Remove(army);
		}

		public MilitaryEntity GetRandomEnemyEntity(ArmyTeam armyAsker)
		{
			var enemyArmy = armies.Find(e => e.Team != armyAsker);

			if (enemyArmy == null)
			{
				return null;
			}

			return enemyArmy.GetRandomEntity();
		}

		public void FinishGame()
		{
			armies.ForEach(e => e.FinishGame());
			var wonArmy = armies.Find(e => e.CheckIsAnyAlive());
			Debug.Log(wonArmy.Team + " Team won the game");
		}

		private void Start()
		{
			armies.Shuffle();
			armies.ForEach(e => e.Initialize());

			armyTurn = armies.First();
			armyTurn.MakeMove();
		}

		private void OnMakeMove()
		{
			armyTurn = armies.Find(e => e.Team != armyTurn.Team);
			armyTurn.MakeMove();
		}
	}
}
