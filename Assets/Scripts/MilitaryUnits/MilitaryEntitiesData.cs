using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AFSInterview.MilitaryUnits
{
	[CreateAssetMenu(fileName = "MilitaryEntitiesData", menuName = "MilitaryEntities/EntitiesData", order = 1)]
	public class MilitaryEntitiesData : ScriptableObject
	{
		[SerializeField] private List<MilitaryEntity> entities = new List<MilitaryEntity>();

		public MilitaryEntity GetPrefabToSpawn(MilitaryType type)
		{
			return entities.Find(e => e.Type == type);
		}
    }
}
