using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace AFSInterview.MilitaryUnits
{
	public class MilitaryEntity : MonoBehaviour
	{
		private const float MINIMAL_ATTACK_DAMAGE = 1f;

		[SerializeField, BoxGroup("Settings")] private MilitaryUnit militaryUnit;

		private float actualHealthValue = 0;
		private int turnCounter = 1;

		public event Action<MilitaryEntity> OnEntityDied;

		public MilitaryType Type => militaryUnit.Type;

		private void Awake()
		{
			actualHealthValue = militaryUnit.HealthPoints;
		}

		public void MakeDamage(MilitaryEntity entity)
		{
			var damage = CalculateDamage(entity);
			Debug.Log(this.gameObject.name + " attacked " + entity.gameObject.name + " and dealt " + damage + " damage");
			entity.RevceiveDamage(damage);

			ResetAttackInvervalCounter();
		}

		public bool CanAttack()
		{
			return turnCounter >= militaryUnit.AttackInterval;
		}

		public void FinishTurn()
		{
			turnCounter++;
		}

		private void RevceiveDamage(float value)
		{
			actualHealthValue -= value;

			if(actualHealthValue <= 0f)
			{
				OnEntityDied?.Invoke(this);
			}
		}

		private float CalculateDamage(MilitaryEntity entity)
		{
			var attackData = militaryUnit.AttackData;
			var specialAttackData = attackData.AttackAttributes.Find(e => e.Attributes.HasFlag(entity.militaryUnit.Attributes));

			float damage = 0f;

			if(specialAttackData != null)
			{
				damage = specialAttackData.AttackDamage - entity.militaryUnit.ArmorPoints;
			}
			else
			{
				damage = attackData.DefaultAttackDamage - entity.militaryUnit.ArmorPoints;
			}

			if(damage < MINIMAL_ATTACK_DAMAGE)
			{
				damage = MINIMAL_ATTACK_DAMAGE;
			}

			return damage;
		}

		private void ResetAttackInvervalCounter()
		{
			turnCounter = 1;
		}
	}
}
