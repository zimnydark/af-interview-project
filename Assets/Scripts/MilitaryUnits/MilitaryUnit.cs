using System;
using System.Collections.Generic;

namespace AFSInterview.MilitaryUnits
{
	public enum MilitaryType
    {
        LongSwordKnight,
        Archer,
        Druid,
        Catapult,
        Ram
    }

    [Flags]
    public enum MilitaryAttributes
    {
        None = 0,
        Light = 1 << 0,
        Armored = 1 << 1,
        Mechanical = 1<< 2,
    }

    [Serializable]
    public class AttributeAttack
    {
        public float AttackDamage;
        public MilitaryAttributes Attributes;
    }

    [Serializable]
    public class MilitaryUnit 
    {
        public MilitaryType Type;
        public MilitaryAttributes Attributes;
        public float HealthPoints;
        public float ArmorPoints;
        public int AttackInterval;
        public AttackData AttackData;
    }

    [Serializable]
    public class AttackData
    {
		public float DefaultAttackDamage;
		public List<AttributeAttack> AttackAttributes;
	}
}
