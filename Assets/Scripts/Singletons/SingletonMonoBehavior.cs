using UnityEngine;

namespace AFSInterview.Singletons
{
	[DisallowMultipleComponent]
	public class SingletonMonoBehavior<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T instance;

		public static T Instance
		{
			get
			{
				if (instance == null)
				{
					instance = FindObjectOfType<T>();
				}

				return instance;
			}
		}

		protected virtual void SingletonAwake() { }
		protected virtual void SingletonDestroyed() { }

		protected virtual void Awake()
		{
			if (instance != null && instance != this)
			{
				Debug.LogWarning("Multiple instances of Singleton<T> found. Destroying the extra one.");
				Destroy(gameObject);
				return;
			}

			instance = this as T;

			SingletonAwake();
		}

		protected virtual void OnDestroy()
		{
			if (instance == this)
			{
				SingletonDestroyed();
				instance = null;
			}
		}
	}
}
