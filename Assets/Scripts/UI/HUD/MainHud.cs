using AFSInterview.Items;
using AFSInterview.Singletons;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace AFSInterview.UI.HUD
{
    public class MainHud : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI moneyValueText;

		private void OnEnable()
		{
			if (InventoryController.Instance != null)
			{
				InventoryController.Instance.OnMoneyValueChanged += UpdateMoneyText;
			}
		}

		protected void OnDisable()
		{
			if (InventoryController.Instance != null)
			{
				InventoryController.Instance.OnMoneyValueChanged -= UpdateMoneyText;
			}
		}

		public void UpdateMoneyText(int value)
        {
			moneyValueText.text = "Money: " + value;
		}
	}
}
